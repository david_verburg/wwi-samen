<?php

include 'includes/connection.php';
include 'header.php';
include 'includes/winkelmandinclude.php'; //dit is nodig voor de winkelmand

if(isset($_GET['zoekop'])){
    $zoekop = $_GET['zoekop'];
}
else{
    $zoekop = "titel";
}

if(!isset($_GET['aantal'])){
    $aantalperpagina = 10;
}
else{
    $aantal = $_GET['aantal'];
    $aantalperpagina =  $aantal ;
}
if(!isset($_GET['pagina'])){
    $pagina = 1;
}
else{
    $pagina = $_GET['pagina'];
}

if(isset($_GET['search'])){
    if(!$_GET['search']==""){
        $gezocht = $_GET['search'];
        $search = mysqli_escape_string($con, $gezocht);
        //print($search . "<BR>"); // toont de ingevoerde waarde

        $search = preg_replace( "#[^0-9a-z]#i"," ",$search);
        if(!is_int($search)){
            //print($search . "<BR>"); // toont of de ingevoerde waarde juist gefilterd is

            $search = explode(" ", $search); // maakt een array van de ingevoerde woorden
            //echo("<BR>");
            //print_r($search); echo("<BR>"); // Laat zien hoe de array nu is

            foreach($search as $s){
                if($s == ""){
                    unset($search[$s]); //Verwijdert de lege waardes
                }
            }

            //print_r($search); echo("<BR>"); //Toont de woorden die je over hebt
            //print(count($search));

            $search = array_values($search); //herindiceert de waardes in $search
            //print_r($search);

            $sql1 = "Select * from stockitems 
                     JOIN stockitemholdings ON stockitemholdings.StockItemID = stockitems.StockItemID  
                     Where ";
            $sql2 = "";
            $sql3 = "";
            $sql4 = "";
            if($zoekop == "nummer"){
                foreach ($search as $s){
                    $sql2 = " stockitems.StockItemID = ". $s;
                }

            }
            if($zoekop =="titel"){
                $sql2 = " StockItemName LIKE '%";

                foreach ($search as $s){
                    if($s == $search[count($search)-1]){
                        $sql3 .= $s;
                    }
                    else{
                        $sql3 .=  $s . "%' AND StockItemName LIKE '%";
                    }

                }
                $sql4 = "%' LIMIT ".$aantalperpagina."
                OFFSET ". ($aantalperpagina * $pagina - $aantalperpagina ) .";";

            }
            $sql = $sql1.$sql2.$sql3.$sql4;
            //print($sql); // Om te kijken of de query gelukt is.


        }
        $R = mysqli_query($con, $sql);

    }

}




?>

<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="shortcut icon" href="WWI%20Logo.ico" />
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/css.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zoeken</title>
    <meta charset="UTF-8">
</head>
<body>

<BR>
<?php
print('<div id="p-flex">');
if(!$_GET['search']==""){
    if($R->num_rows<1){
        echo("Geen resultaten voor uw zoekopdracht: ".$gezocht);
    }
    $aantalproducten = 0;
    if(isset($_GET['search'])&&!$_GET['search']==""){
        while ($rowproduct = mysqli_fetch_array($R)) {
            $aantalproducten++;
            $product = $rowproduct['StockItemName'];
            $prijs = $rowproduct['RecommendedRetailPrice'];
            $beschrijving = $rowproduct['MarketingComments'];
            $productID = $rowproduct['StockItemID'];
            $btw = $rowproduct['TaxRate'];
            $prijsplusbtw = $prijs * (1+ $btw/100);
            $prijsplusbtw = round($prijsplusbtw , 2);
            $voorraad = $rowproduct['QuantityOnHand'];

            echo('<div class="p-flex">
        <div class="p-flex-in">
            <img class="p-img" src="data:image/jpeg;base64,'.base64_encode( $rowproduct['Photo'] ).'"/>
            <div class="p-name">'.$product.'</div>
            <div class="p-price">&euro; '.$prijs.'</div>
            <div class="p-price">(&euro; '.$prijsplusbtw .' inc btw)' . '</div>
            <div class="p-desc">'.$beschrijving.'</div>
            <div class="p-desc"><span>Voorraad: </span>' . $voorraad . '</div>
            <form action = zoek.php >
            <button class="p-add"  name = "product" value="'.$productID.'" type = "submit" >Bekijk meer</button>
            </form>
            
            <form method="get" action="zoek.php?action=add&StockItemID='.$rowproduct['StockItemID']. '"/>
                <input type="text" name="aantalproducten" value="1" class="form-control" style="margin: 0px 5px 5px 0x;" />
                <input type="hidden" name="search" value= "' . $gezocht . '">
                <input type="submit" name="add_to_cart" style="
            background: #0051ff;
            color: #fff;
            border: none;
            width: 100%;
            padding: 10px;
            margin: 10px 5px 5px 5px;
            font-size: 1.1em;
            font-weight: bold;
            cursor: pointer;
            " value="In Winkelmand" />
            </form>
        </div>
    </div>');

        }
}

    $sqlcount = "SELECT count(*) 
FROM wideworldimporters.stockitems 
WHERE stockitems.stockitemname LIKE '%".$gezocht."%' ;";
//print($sqlcount);
    $count = mysqli_query($con, $sqlcount);
    $count = mysqli_fetch_array($count);
//print_r($count);
    $tel = $count['count(*)'];
//print($tel);
    for($i= 1; $i <= ceil($tel/$aantalperpagina);$i++){
        if(!($i==$pagina)){
            echo('<form action = "Zoek.php" method = "get">');
            print('
            <input type = "hidden" value = "'.$aantalperpagina.'" name = "aantal">
            <input type = "hidden" value =  "'.$gezocht.'" name = "search">
            <input type="submit" name = "pagina" value = "'.$i.'">
            ');
            echo('</form>');
        }
        else{
            print('...');
        }

    }

//echo($aantalproducten);
    $sqlcount = "SELECT count(*) 
FROM wideworldimporters.stockitems 
WHERE stockitems.stockitemname LIKE '%".$gezocht."%' ;";
//print($sqlcount);
    $count = mysqli_query($con, $sqlcount);
    $count = mysqli_fetch_array($count);
//print_r($count);
    $tel = $count['count(*)'];
//print($tel);
    ?>

    <form method="get" action="zoek.php">
        <input type = "hidden" value = "<?php echo($gezocht) ?>" name = "search">
        <input type = "hidden" value = 1 name = "pagina">
        <select name="aantal">
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select>
        <input width="300px" type="submit" value = "OK">
        <BR>
    </form>
    </div>
    <?php
}
else{
    echo("Geen zoekterm ingevoerd.");
}





?>
<BR><BR>
</div>



<BR>


</body>
</html>











