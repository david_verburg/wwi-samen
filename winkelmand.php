<?php
include 'includes/connection.php';
include 'header.php';
include 'includes/winkelmandinclude.php'; //dit is nodig voor de winkelmand

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Uw winkelmand</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</head>
	<body>

			<h3>Winkelmand</h3>
			<div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<th width="30%">Productnaam</th>
						<th width="10%">Aantal</th>
						<th width="10%">Prijs ex btw</th>
                        <th width="10%">btw</th>
                        <th width="10%">Prijs inc btw</th>
						<th width="5%">Actie</th>
					</tr>
					<?php
					if(!empty($_SESSION["Winkelmand"]))
					{
                        $totaalexbtw = 0;
					    $totaalbtw = 0;
					    $totaal = 0;
						foreach($_SESSION["Winkelmand"] as $keys => $values)
						{
						    //print_r($values);
						    $sql = "SELECT * from stockitems where StockItemID = ".$values['productnummer'].";";
                            if($resultaat = mysqli_query($con, $sql)){ // voert de query in
                                if(mysqli_num_rows($resultaat) > 0){ //kijkt of er resultaten zijn
                                    //print_r($resultaat); //Test of de producten worden opgehaald
                                }
                                while ($row = mysqli_fetch_array($resultaat)) {
                                    $naam = $row['StockItemName'];
                                    $prijsexbtw = $row['RecommendedRetailPrice'];
                                    $btwperc = round($row['TaxRate'],1);
                                    $btwbedr = round($prijsexbtw*$btwperc/100,2);
                                    $prijsincbtw = $prijsexbtw + $btwbedr;

                                }
                            }
					?>
					<tr>
						<td><?php echo $naam; ?></td>
						<td><?php echo $values["aantalproducten"]; ?></td>
                        <td>€ <?php echo $prijsexbtw * $values["aantalproducten"]; ?></td>
                        <td>€ <?php echo ($btwbedr * $values["aantalproducten"] . " (".$btwperc."%)"); ?></td>
						<td>€ <?php echo number_format($values["aantalproducten"] * $prijsincbtw, 2);?></td>

						<td><a href="winkelmand.php?action=delete&StockItemID=<?php echo $values["productnummer"]; ?>"><span class="text-danger">Verwijderen</span></a></td>
					</tr>
					<?php
                            $totaalbtw = number_format(round(($totaalbtw + $btwbedr * $values["aantalproducten"]),2),2);
                            $totaalexbtw = number_format(round(($totaalexbtw + $prijsexbtw * $values["aantalproducten"]),2),2);
						$_SESSION["totaalprijs"] =	$totaal = number_format(round(($totaal + $prijsincbtw * $values["aantalproducten"]),2),2);
						}
					?>
					<tr>

						<td colspan="2" align="right"><strong>Totaal</strong></td>
                        <td align="right"><strong>€ <?php echo $totaalexbtw; ?></strong></td>
                        <td align="right"><strong>€ <?php echo $totaalbtw; ?></strong></td>
						<td align="right"><strong>€ <?php echo $totaal; ?></strong></td>
						<td></td>
					</tr>
					<?php
					}
					?>

				</table>
			</div>
			<a href="bestellen.html"> Bestellen </a>
	</body>
</html>

