<?php
session_start();
require_once 'Functies.php';
$DBHost = 'localhost';
$DBUser = 'root';
$dbwachtwoord = '';
$databasenaam = 'wideworldimporters';

$con = mysqli_connect($DBHost, $DBUser, $dbwachtwoord, $databasenaam);
if (mysqli_connect_errno()) {
    die('Kon niet verbinden met database' . mysqli_connect_error());}
if ( !isset($_POST['gebruikersnaam'], $_POST['wachtwoord']) ) {
    // Could not get the data that should have been sent.
    die ('Please fill both the username and password field!');
}
if ($stmt = $con->prepare('SELECT id, wachtwoord, voornaam, achternaam FROM accounts WHERE gebruikersnaam = ?')) {
    // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
    $stmt->bind_param('s', $_POST['gebruikersnaam']);
    $stmt->execute();
    // Store the result so we can check if the account exists in the database.
    $stmt->store_result();
}

if ($stmt->num_rows > 0) {
    $stmt->bind_result($id, $wachtwoord,$voornaam,$achternaam);
    $stmt->fetch();
    // Account exists, now we verify the password.
    // Note: remember to use password_hash in your registration file to store the hashed passwords.
    if ($_POST['wachtwoord'] === $wachtwoord) {
        // Verification success! User has loggedin!
        // Create sessions so we know the user is logged in, they basically act like cookies but remember the data on the server.
        session_regenerate_id();
        $_SESSION['loggedin'] = TRUE;
        $_SESSION['name'] = $voornaam .' ' . $achternaam;
        $_SESSION['gebruikersnaam'] = $_POST['gebruikersnaam'];
        $_SESSION['id'] = $id;
        print ('Welcome ' . $voornaam .' ' . $achternaam );
        header('refresh: 2;url=Profiel.php');

    } else {
        echo 'Wachtwoord onjuist';
    }
} else {
    echo 'Gebruikersnaam onjuist';
}
$stmt->close();
?>