
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <title>Login </title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
        <link href="login.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="login">
			<h1>Login</h1>
			<form action="Authenticate.php" method="post">
				<label for="gebruikersnaam">
					<i class="fas fa-user"></i>
				</label>
				<input type="text" name="gebruikersnaam" placeholder="gebruikersnaam" id="gebruikersnaam" required>
				<label for="wachtwoord">
					<i class="fas fa-lock"></i>
				</label>
				<input type="password" name="wachtwoord" placeholder="wachtwoord" id="wachtwoord" required>
				<input type="submit" value="Login">
			</form>
            <a href="http://localhost/wwi-samen/Registreren.php"> Registeren </a>     <a href="http://localhost/wwi-samen/index.php"> Homepagina </a>
		</div>
	</body>
</html>