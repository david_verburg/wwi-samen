<?php
session_start();
$DBHost = 'localhost';
$DBUser = 'root';
$dbwachtwoord = '';
$databasenaam = 'wideworldimporters';

$con = mysqli_connect($DBHost, $DBUser, $dbwachtwoord, $databasenaam);
if (mysqli_connect_errno()) {
    die('Kon niet verbinden met database' . mysqli_connect_error());}
if (!isset($_SESSION['loggedin'])) {
    header('Location: index.html');
    exit();
}
$stmt = $con->prepare('select email,adres,postcode,Favproduct from accounts where id = ?');
$stmt->bind_param('i',$_SESSION['id']);
$stmt->execute();
$stmt->bind_result($email,$adres,$postcode,$favproduct);
$stmt->fetch();
$stmt->close();
$product = explode(";",$favproduct);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Profile Page</title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>
<body class="loggedin">
<nav class="navtop">
    <div>
        <h1>Website Title</h1>
        <a href="Profiel.php"><i class="fas fa-user-circle"></i>Profile</a>
        <a href="logout.php"><i class="fas fa-sign-out-alt"></i>Logout</a>
        <a href="http://localhost/wwi-samen/index.php"> Homepagina </a>
    </div>
</nav>
<div class="content">
    <h2>Profiel pagina</h2>
    <div>
        <p>Dit zijn uw gegevens:</p>
        <table>
            <tr>
                <td>Gebruikersnaam</td>
                <td><?= $_SESSION['gebruikersnaam']?></td>
            </tr>
            <tr>
                <td>Naam</td>
                <td><?=$_SESSION['name']?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?=$email?></td>
            </tr>
            <tr>
                <td>Adres</td>
                <td><?=$adres?></td>
            </tr>
            <tr>
                <td>Postcode</td>
                <td><?=$postcode?></td>
            </tr>
            <tr>
                <td>Favoriete producten</td>
                <td><?php foreach ($product as $key => $value){
                    $querywhere="$value";
                    $stmt2 = $con->prepare('select StockItemName,RecommendedRetailPrice from stockitems where StockitemID = ?');
                    $stmt2->bind_param("s", $value);
                    $stmt2->execute();
                    $stmt2->bind_result($Productnaam,$prijs);
                    $stmt2->fetch();
                    $stmt2->close();
                    print ($Productnaam . ' <br>' . $prijs . '<br>');
                    }?>
                </td>


            </tr>

        </table>
    </div>
</div>
</body>
</html>