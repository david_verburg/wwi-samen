<?php
include 'includes/connection.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Geplaatste bestellingen</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<h3>Geplaatste bestellingen</h3>
<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th width="10%">OrderID</th>
            <th width="20%">KlantNaam</th>
            <th width="20%">Datum van bestellen</th>
            <th width="20%">btw</th>
            <th width="10%">Prijs inc btw</th>
        </tr>
<?php
        $ordersql = mysqli_query($con, "SELECT orderid FROM orders ORDER BY orderid DESC LIMIT 1");

        while ($row = $ordersql->fetch_assoc()) {
            $orderid = $row['orderid'];
        }

        $lastorder = $orderid;

        echo "Old lastorder = " . $lastorder . "<br><br>";

        for($x = 1; $x <= 250; $x++) {

            /*echo "Lastorder ordernumber ". $x . " = " . $lastorder . "<br>";*/

            $sql = "SELECT * from orders inner join customers on orders.customerid=customers.customerid where orderid = '$lastorder';";
            if ($resultaat = mysqli_query($con, $sql)) {
                while ($row = mysqli_fetch_array($resultaat)) {
                    $orderid = $row['OrderID'];
                    $klantnaam = $row['CustomerName'];
                    $orderdate = $row['OrderDate'];
                }
            }
            $lastorder = $lastorder - 1;
        ?>
                <tr>
                    <td><?php echo $orderid; ?></td>
                    <td><?php echo $klantnaam; ?></td>
                    <td><?php echo $orderdate; ?></td>
                    <td><?php echo 1; ?></td>
                    <td><?php echo 2;?></td>
                </tr>
        <?php
        }
        ?>
    </table>
</div>
</body>
</html>