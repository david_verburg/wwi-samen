
	CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `gebruikersnaam` varchar(20) NOT NULL,
  `wachtwoord` varchar(20) NOT NULL,
  `voornaam` varchar(20) NOT NULL,
  `achternaam` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `adres` varchar(100) DEFAULT NULL,
  `postcode` varchar(6) DEFAULT NULL,
  `bestellingen` int(11) DEFAULT NULL,
  `Favproduct` varchar(1000) DEFAULT NULL,
  `telefoonnummer` int(12) DEFAULT NULL,
  `bedrijf` varchar(100) DEFAULT NULL,
  `accountgemaaktop` datetime DEFAULT NULL,
  `accounttype` int(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

                    
			

