<?php
include 'includes/connection.php';
include 'header.php';
include 'includes/winkelmandinclude.php';
?>
<!DOCTYPE html>
<?php
$product = $_GET['product'];

$sql = "SELECT * FROM wideworldimporters.stockitems
JOIN wideworldimporters.videos ON stockitems.StockItemID = videos.ProductID
JOIN wideworldimporters.stockitemholdings ON stockitems.StockItemID = stockitemholdings.StockItemID Where stockitems.stockitemID =". $product ." ;";



$kleursql = "SELECT ColorName FROM wideworldimporters.colors C 
             JOIN wideworldimporters.stockitems SI ON C.ColorID = SI.ColorID LIMIT 1 ;";

$reviewsql = "SELECT * FROM wideworldimporters.reviews WHERE ProductNummer = ". $product .";";


$result = $con->query($sql);
while($row = $result->fetch_assoc()) {
    $nummer = $row["StockItemID"];
    $naam = $row["StockItemName"];
    $merk = $row["Brand"];
    $afmetingen = $row["Size"];
    $gekoeld = $row["IsChillerStock"];
    $code = $row["Barcode"];
    $btw = $row["TaxRate"];
    $prijszonderbtw = $row["RecommendedRetailPrice"];
    $btw = round($prijszonderbtw * $btw / 100, 2);
    $prijs = round($prijszonderbtw + $btw , 2);

    $video = $row["Video"]; // Moet nog worden aangepast, want er mag geen extra kolom worden toegevoegd in een tabel

    $voorraad = $row["QuantityOnHand"];
    $levertijd = $row["LeadTimeDays"];
     $gewicht = $row["TypicalWeightPerUnit"];
    $gewicht = str_replace('.', ',',$gewicht);
    $Marketing = $row["MarketingComments"];
    $GemaaktIn = $row["CustomFields"];
    $GemaaktIn = strstr($GemaaktIn,'{ "CountryOfManufacture": "' );
    $GemaaktIn = explode('",', $GemaaktIn);
    $GemaaktIn = $GemaaktIn[0];
    $GemaaktIn = strstr($GemaaktIn, ': "');
    $GemaaktIn = ltrim($GemaaktIn, ': "');
    //$GemaaktIn = str_replace('{ "CountryOfManufacture": "', " ", $GemaaktIn);
    //$GemaaktIn = strstr($GemaaktIn, 'China');
    $Tags = $row["Tags"];
    $Details = $row["SearchDetails"];

    $catsql = "SELECT DISTINCT StockGroupName FROM wideworldimporters.stockgroups SG
               JOIN wideworldimporters.stockitemstockgroups SISG on SG.StockGroupID = SISG.StockGroupID
               JOIN wideworldimporters.stockitems SI on SISG.StockItemID = SI.StockItemID
               WHERE SI.StockItemID = ".$nummer." LIMIT 1;";

               $Categorie = $con->query($catsql);


}
?>
<html>
<head>
    <link rel="stylesheet" href="css/css.css" type="text/css">
    <title><?php echo($naam) ?></title>
    <h1 align = "center"><?php echo($naam); ?></h1>
</head>
<body>
    <?php    // Check connection
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    ?>

    <div class = "productbox" align = "center">
        <div class="p-flex">
            <div class="p-flex-in">

                <?php
                if($result = mysqli_query($con, $sql)){
                    if(mysqli_num_rows($result) > 0){

                        echo("<BR>");
                        while($row = mysqli_fetch_array($result)){
                            echo '<div class ="foto"><img src="data:image/jpeg;base64,'.base64_encode( $row['Photo'] ).'" /></div>';
                        }
                        mysqli_free_result($result);
                    }
                }

                echo("<BR>");
                echo("<strong>Prijs: &euro; ". $prijs ."</strong><BR>");
                echo("Prijs zonder btw: &euro; " . $prijszonderbtw);
                echo("<BR>");
                echo("Btw:  &euro; " . $btw . "<BR>" . "<BR>");

                if(!($Marketing == "")){
                    echo("<strong> Beschrijving: </strong> <BR>" . $Marketing . "<BR>" . $Details . "<BR>");

                }
                if(!($merk == "")){
                    echo("Merk: " . $merk);
                    echo("<BR>");
                }

                echo("<BR>");
                echo("Voorraad: " . $voorraad);
                echo("<BR>");
                echo("Levertijd: " . $levertijd . " dagen");
                echo("<BR>");



                echo("<BR><strong>Specificaties: </strong><BR>");

                if($kleurresult = mysqli_query($con, $kleursql)){
                    while($row = $kleurresult->fetch_assoc()){
                        print("Kleur: " . $row["ColorName"] . "<BR>");
                    }
                }





                if(!($afmetingen == "")){
                    echo("Afmetingen: " . $afmetingen . "<BR>");
                }
                else{
                    echo("Afmetingen : niet bekend <BR>");
                }

                if(!($code == "")){
                    echo("Code: " . $code);
                }
                else{
                    echo("Code: niet bekend");
                }
                echo("<BR>");
                echo("Gewicht: " . $gewicht . " kilogram" . "<BR>");


                echo("<BR>");
                echo("Gemaakt in: " . $GemaaktIn. "<BR>");
                //echo("Tags: " . $Tags . "<BR>"); < Kan later iets mee gedaan worden.

                echo("<p style= 'color='#8b0000'> Bij al onze producen zit standaard 2 jaar garantie! Niet goed? Geld terug!</p> <br>");


                if($gekoeld == 1){
                    echo("<BR><strong>Dit product is gekoeld!</strong>");
                }

                while($row = $Categorie->fetch_assoc()){
                    print("Categorie: " . $row["StockGroupName"]. "<BR>");
                }

                echo('<br>');
                echo('<form method="post" action="product.php?action=add&StockItemID='.$nummer.'&product=' . $nummer . '"/>');

            echo('<input type="number" name="aantalproducten" min="1" value="1" class="form-control" style="margin: 0px 5px 5px 0x;" />');

            echo('<input type="submit" name="Toevoegen" style=
            "
            background: #0051ff;
            color: #fff;
            border: none;
            width: 100%;
            padding: 10px;
            margin: 10px 5px 5px 5px;
            font-size: 1.1em;
            font-weight: bold;
            cursor: pointer;
            "
            value="In Winkelmand" />');


            echo('</form>');
            echo('<a href="reviewform.php?productid='.$nummer.'"> <button class="p-add">Schrijf een review</button> </a>');
            print("<table>");
            print("<tr>");
            print("<th>Naam</th>");
            print("<th>Aanbeveling</th>");
            print("<th>Review</th>");
            print("</tr>");
            if($reviewresult = mysqli_query($con, $reviewsql)){
                while($row = $reviewresult->fetch_assoc()){
                    print ("<tr>");
                    print("<td>" . $row["Bezoekernaam"] . "</td>");
                    print("<td>" . $row["Aanbeveling"] . "</td>");
                    print("<td>" . $row["Review"] . "</td>");
                    print ("</tr>");
                }
            }
            print("</table>");    


            echo('<center><iframe width="100%" height="500px" src="https://www.youtube.com/embed/');
            echo($video .'"></center>');


                ?>




           

            

            


            </div>
        </div>
    </div>


<br><br><br><br><br>
</body>
</html>


