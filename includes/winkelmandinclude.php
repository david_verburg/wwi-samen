<?php
session_start();
if(isset($_POST["Toevoegen"]))
{
    if(isset($_SESSION["Winkelmand"]))
    {
        $nummers = array_column($_SESSION["Winkelmand"], "productnummer");
        if(!in_array($_GET["StockItemID"], $nummers)) //Kijkt of product al bestaan in de array
        {
            $productarray = array( //maakt een extra array aan met het product en aantal
                'productnummer'	=>	$_GET["StockItemID"],
                'aantalproducten'		=>	$_POST["aantalproducten"]
            );
            $_SESSION["Winkelmand"][count($_SESSION["Winkelmand"])] = $productarray; //voegt de array toe na alle andere
        }
        else
        {
            echo '<script>alert("Product bestaat al in winkelmand")</script>';
        }
    }
    else
    {
        $productarray = array( //maakt de eerste array aan met het product en aantal
            'productnummer'			=>	$_GET["StockItemID"],
            'aantalproducten'		=>	$_POST["aantalproducten"]
        );
        $_SESSION["Winkelmand"][0] = $productarray; //zet de eerste array op plaats 0 in de winkelmand
    }
}

if(isset($_GET["action"])) //verwijdert het product
{
    if($_GET["action"] == "delete")
    {
        foreach($_SESSION["Winkelmand"] as $keys => $values)
        {
            if($values["productnummer"] == $_GET["StockItemID"])
            {
                unset($_SESSION["Winkelmand"][$keys]);
                echo '<script>alert("Product verwijderd")</script>';
                echo '<script>window.location="winkelmand.php"</script>';
            }
        }
    }
}
?>
