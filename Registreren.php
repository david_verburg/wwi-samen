<?php
include 'includes/connection.php';
include 'header.php';
include 'includes/winkelmandinclude.php';

//print_r($_POST); // Om te kijken wat er al is.
$gegevens= array();
$aantalgoedingevuld = 0;
$volledigenaam = $voornaam = $achternaam = $email = $wachtwoord = $telefoonnummer = $adres = $postcodeletters = $postcodenummers = $woonplaats = "";
$naamerror = "";
$voornaamerror = "";
$achternaamerror = "";
$emailerror = "";
$wachtwoorderror = "Minimaal 8 karakters.";
$telefoonnummererror = "";
$adreserror = "";
$postcodenummerserror = "";
$postcodeletterserror = "";
$woonplaatserror = "";

// BEGIN VALIDATIE //

if ($_SERVER["REQUEST_METHOD"] == "POST") { //Kijkt of er gebruik wordt gemaakt van POST (oftewel of submit hieronder is gebruikt)


    if (empty($_POST["volledigenaam"])) { //Kijkt of de naam is ingevuld
    $naamerror = "Vul uw naam in a.u.b.";
    }
    else {
        $volledigenaam = $_POST['volledigenaam'];

        if (preg_match("/^[a-zA-Z -]*$/",$volledigenaam)) { //Kijkt of er alleen letters of spaties zijn gebruikt
            $aantalgoedingevuld++;
            $_SESSION['volledigenaam'] = $volledigenaam;
        }
        else{
            $naamerror = "Alleen letters en spaties";
        }
    }

    if (empty($_POST["voornaam"])) { //Kijkt of de voornaam is ingevuld
        $voornaamerror = "Vul uw voornaam in a.u.b.";
    }
    else {
        $voornaam = $_POST['voornaam'];
        if (preg_match("/^[a-zA-Z-]*$/",$voornaam)) { // Kijkt of alleen letters of streepjes zijn gebruikt
            $aantalgoedingevuld++;
            $_SESSION['voornaam'] = $voornaam;
        }
        else{
            $voornaamerror = "Alleen letters en spaties";
        }
    }

    if (empty($_POST["achternaam"])) { //Kijkt of de achternaam is ingevuld
        $achternaamerror = "Vul uw achternaam in a.u.b.";
    }
    else {
        $achternaam = $_POST['achternaam'];
        if (preg_match("/^[a-zA-Z -]/",$achternaam)) { // Kijkt of alleen Letters, streepjes of spaties zijn gebruikt
            $aantalgoedingevuld++;
            $_SESSION['achternaam'] = $achternaam;
        }
        else{
            $achternaamerror = "Alleen letters en spaties";
        }
    }

    if (empty($_POST["email"])) { //Kijkt of de email is ingevuld
        $emailerror = "Vul uw email in a.u.b.";
    }
    else {
        $email = $_POST['email']; //Kijkt of de email voldoet aan de eisen

        if (preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/",$email)) { // Kijkt of de email geldig is, nl: naam + @ + domeinnaam + . + extensie (2 tot 5 letters)
            $emailsql = 'Select email FROM accounts where email = "'.$email.'";';
            $bestaatemailal = mysqli_query($con, $emailsql);
            if ($bestaatemailal->num_rows>0){
                $emailerror = "email bestaat al";
            }
            else{
                $aantalgoedingevuld++;
                $_SESSION['email'] = $email;
            }

        }
        else{
            $emailerror = "Geen geldige email";
        }
    }

    if (empty($_POST["wachtwoord"])) { //Kijkt of het wachtwoord is ingevuld
        $wachtwoorderror = "Vul een wachtwoord in a.u.b.";
    }
    else {
        $wachtwoord = $_POST['wachtwoord']; //Kijkt of het wachtwoord voldoet aan de eisen
        if (preg_match("/^[a-zA-Z0-9.!@#$ ]{8,1000}$/",$wachtwoord)) {
            $aantalgoedingevuld++;
            $_SESSION['wachtwoord'] = $wachtwoord;
        }
        else{
            $wachtwoorderror = "Wachtwoord moet minimaal 8 tekens lang zijn";
        }
    }

    if (empty($_POST["telefoonnummer"])) { //Kijkt of het telefoonnummer is ingevuld
        $telefoonnummererror = "Vul uw telefoonnummer in a.u.b.";
    }
    else {
        $telefoonnummer = $_POST['telefoonnummer']; //Kijkt of het telefoonnummer voldoet aan de eisen
        if (preg_match("/^[0-9]{6,12}$/",$telefoonnummer)) {
            $aantalgoedingevuld++;
            $_SESSION['telefoonnummer'] = $telefoonnummer;
        }
        else{
            $telefoonnummererror = "Telefoonnummer niet goed";
        }
    }

    if (empty($_POST["adres"])) { //Kijkt of het adres is ingevuld
        $adreserror = "Vul uw adres in a.u.b.";
    }
    else {
        $adres = $_POST['adres']; //Kijkt of het adres voldoet aan de eisen
        if (preg_match("/^[a-zA-Z ]{1,50}+[0-9]{1,3}$/",$adres)) {
            $aantalgoedingevuld++;
            $_SESSION['adres'] = $adres;
        }
        else{
            $adreserror = "Onjuiste adresgegevens";
        }
    }

    if (empty($_POST["postcodenummers"])) { //Kijkt of postcodenummers zijn ingevuld
        $postcodenummerserror = "Vul uw postcodenummers in a.u.b. <BR>";
    }
    else {
        $postcodenummers = $_POST['postcodenummers']; //Kijkt of de postcodenummers voldoen aan de eisen
        if (preg_match("/[0-9]{4}$/",$postcodenummers)) {
            $aantalgoedingevuld++;
            $_SESSION['postcodenummers'] = $postcodenummers;
        }
        else{
            $postcodenummerserror = "Postcodenummers niet goed ingevuld";
        }
    }

    if (empty($_POST["postcodeletters"])) { //Kijkt of postcodeletters zijn ingevuld
        $postcodeletterserror = "Vul uw postcodeletters in a.u.b. <BR>";
    }
    else {
        $postcodeletters = $_POST['postcodeletters']; //Kijkt of de postcodeletters voldoen aan de eisen
        if (preg_match("/^[a-zA-Z]{2}$/",$postcodeletters)) {
            $aantalgoedingevuld++;
            $_SESSION['postcodeletters'] = $postcodeletters;
        }
        else{
            $postcodeletterserror = "Voer twee postcodeletters in a.u.b.";
        }
    }



    if (empty($_POST["woonplaats"])) { //Kijkt of de woonplaats is ingevuld
        $woonplaatserror = "Vul uw woonplaats in a.u.b.";
    }
    else {
        $woonplaats = $_POST['woonplaats'];
        if (preg_match("/^[a-zA-Z ]{2,70}$/",$woonplaats)) { //Kijkt of de woonplaats voldoen aan de eisen
            $aantalgoedingevuld++;
            $_SESSION['woonplaats'] = $woonplaats;
        }
        else{
            $woonplaatserror = "Woonplaats niet goed ingevuld";
        }
    }

    if($aantalgoedingevuld == 10){
        header("Location: registratieafhandelen.php");
    }
}

// EINDE VALIDATIE //

?>
<!doctype html>
<html lang="en">
<head>

    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Maak uw account aan</title>
</head>
<body>
<div class="registratieformulier">
    <div class="rij">
        <form action="Registreren.php" method="post">
            <p><strong>Uw gegevens:</strong></p>
            <p>Gebruikersnaam:</p>
            <input type = "text" name = "volledigenaam" value="<?php echo($volledigenaam)?>" >
            <?php echo($naamerror)?>

            <p>Voornaam:</p>
            <input type = "text" name = "voornaam" value="<?php echo($voornaam)?>" title = "voornaam">
            <?php echo($voornaamerror)?>

            <p>Achternaam:</p>
            <input type = "text" name = "achternaam" value="<?php echo($achternaam)?>" >
            <?php echo($achternaamerror)?>

            <p>Email:</p>
            <input type="text" id="email" name="email" value="<?php echo($email)?>">
            <?php echo($emailerror)?>

            <p>Wachtwoord:</p>
            <input type = "password" name = "wachtwoord"  value ="<?php echo($wachtwoord)?>"  pattern=".{8,}" >
            <?php echo($wachtwoorderror)?>

            <p>Telefoonnummer:</p>
            <input type = "text" name = "telefoonnummer" value="<?php echo($telefoonnummer)?>" maxlength = "12">
            <?php echo($telefoonnummererror)?>

            <p>Adres:</p>
            <input type = "text" name = "adres" value="<?php echo($adres)?>">
            <?php echo($adreserror)?>

            <p>Postcode:</p>
            <input type = "text" name = "postcodenummers" value="<?php echo($postcodenummers)?>" maxlength="4" style = "width: 110px" >
            <input type = "text" name = "postcodeletters" value="<?php echo($postcodeletters)?>" maxlength="2" style = "width: 30px">

            <BR>
            <?php echo($postcodenummerserror)?>
            <?php echo($postcodeletterserror)?>

            <p>Woonplaats:</p>
            <input type = "text" name = "woonplaats" value="<?php echo($woonplaats)?>" pattern = "[a-zA-z -']{2,70}">
            <?php echo($woonplaatserror)?>
        </div>
    <div class="rij">
        <!--
            <strong><p>Bezorgadres:</p></strong>
        <p>Adres:</p>
            <input type = "text" name = "bezorgadres">
            <p>Postcode:</p>
            <input type = "text" name = "bezorgpostcode">
            <p>Woonplaats:</p>
            <input type = "text" name = "bezorgplaats">
            <BR>
            <p><input type = "checkbox" name = "voorwaarden" value="checked">
            Ik ga akkoord met de voorwaarden</p>
            <BR>
            -->
            <input type ="submit" name = "submit" value = "Verzenden">


        </form>
    </div>
</div>



<BR>

<BR>
<BR>

</body>
</html>
<?php
//print($aantalgoedingevuld);
include ('includes/footer.html');

?>
